#preparing data for ANN

#importing basic libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#importing dataset
dataset = pd.read_csv("Churn_Modelling.csv")
X = dataset.iloc[:,3:13].values
y = dataset.iloc[:,13].values

# Encoding  data
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
labelencoder_X_1 = LabelEncoder()
X[:, 1] = labelencoder_X_1.fit_transform(X[:, 1])
labelencoder_X_2 = LabelEncoder()
X[:, 2] = labelencoder_X_2.fit_transform(X[:, 2])
onehotencoder = OneHotEncoder(categorical_features = [1])
X = onehotencoder.fit_transform(X).toarray()
X = X[:, 1:]

# Splitting the dataset into the Training set and Test set
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, random_state = 0)

# Feature Scaling
from sklearn.preprocessing import StandardScaler
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

#starting to make ANN from now

#importing libraries for Neural networks
import keras
from keras.models import Sequential
from keras.layers import Dense

#initializing the ANN

classifier = Sequential()

#adding imput layer and first hidden layer(still considered shallow learning)
classifier.add(Dense(6,input_shape =(11),kernel_initializer='uniform', activation= 'relu'))

#adding second hidden layer
classifier.add(Dense(6,kernel_initializer='uniform', activation= 'relu'))

#adding output layer, for more than 2 outputs we have to use softmax as activation
classifier.add(Dense(1,kernel_initializer='uniform', activation= 'sigmoid'))

#compiling the ANN, applying Stochastic Gradient Descent(adam) as bpp 
#categorical_crossentropy should be used as a loss function if using more than one outputs
classifier.compile(optimizer = "adam",loss ="binary_crossentropy", metrics = ['accuracy'] )

#fitting the ANN
classifier.fit(X_train,y_train, batch_size = 5, epochs = 100)

#predicting my test set
y_pred = classifier.predict(X_test)

#choosing threshold to put a binary output
y_pred = (y_pred > 0.5)

#creating confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test,y_pred)
#we got an accuracy of 86% on Training set and 85% on test set